﻿namespace ConfigurationProvidersLibrary.Enums
{
    public enum ProviderType
    {
        File,
        ConfigurationManager
    }
}
