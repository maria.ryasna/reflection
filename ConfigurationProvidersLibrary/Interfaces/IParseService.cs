﻿using System.Reflection;

namespace ConfigurationProvidersLibrary.Interfaces
{
    public interface IParseService
    {
        object Parse(PropertyInfo info, string value);
    }
}
