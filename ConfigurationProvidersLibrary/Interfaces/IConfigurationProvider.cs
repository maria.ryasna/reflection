﻿using ConfigurationProvidersLibrary.Enums;
using System.Reflection;

namespace ConfigurationProvidersLibrary.Interfaces
{
    public interface IConfigurationProvider
    {
        ProviderType ProviderType { get; }
        IParseService ParseService { get; set; }
        string Path { get; set; }
        object LoadSettings(PropertyInfo propInfo, string settingName);
        void SaveSettings(string settingName, object value);
    }
}
