﻿using Reflection.Entities;
using Reflection.Entities.Services;
using Reflection.Interfaces;
using System;

namespace Reflection
{
    internal class Program
    {
        static void Main(string[] args)
        {
            IProviderFactory providers = new ProviderFactory();
            ComponentBase entity = new ComponentBase("Default", TimeSpan.MinValue, 0, 0f, providers);

            Console.WriteLine("Properties before changes:");
            ShowProperties(entity);

            /// From App.config loading Name and IntNumber properties with default values:
            /// Name = 'John'
            /// IntNumber = 10
            /// 
            /// From FileConfig.txt loading Time and FloatNumber properties with default values:
            /// Time = 1.00:00:00
            /// FloatNumber = 12.5
            entity.LoadSettings();

            Console.WriteLine("Properties after init load:");
            ShowProperties(entity);

            /// Make some changes in properties and than save new information
            entity.Name = "Ann";
            entity.Time = TimeSpan.Parse("18:00");
            entity.IntNumber = 11;
            entity.FloatNumber = 22.5f;

            entity.SaveSettings();

            /// Now in file and App.config new values
            /// Change properties one more and than load

            entity.Name = "None";
            entity.Time = TimeSpan.Parse("21:00");
            entity.IntNumber = 12;
            entity.FloatNumber = 32.5f;

            Console.WriteLine("Properties after change -> save -> change:");
            ShowProperties(entity);

            entity.LoadSettings();

            Console.WriteLine("Properties after load:");
            ShowProperties(entity);

            /// To default states
            entity.Name = "John";
            entity.Time = TimeSpan.Parse("1.00:00:00");
            entity.IntNumber = 10;
            entity.FloatNumber = 12.5f;

            entity.SaveSettings();

            Console.ReadKey();
        }

        public static void ShowProperties(ComponentBase component)
        {
            Console.WriteLine(" Name: {0}", component.Name);
            Console.WriteLine(" IntNumber: {0}", component.IntNumber);
            Console.WriteLine(" FloatNumber: {0}", component.FloatNumber);
            Console.WriteLine(" Time: {0}", component.Time);
        }
    }
}
