﻿using ConfigurationProvidersLibrary.Enums;
using Reflection.Entities.Attributes;
using Reflection.Interfaces;
using System;

namespace Reflection.Entities
{
    public class ComponentBase : ConfigurationComponentBase
    {
        [ConfigurationItem("Name", ProviderType.ConfigurationManager)]
        public string Name { get; set; }

        [ConfigurationItem("Time", ProviderType.File)]
        public TimeSpan Time { get; set; }

        [ConfigurationItem("IntNumber", ProviderType.ConfigurationManager)]
        public int IntNumber { get; set; }

        [ConfigurationItem("FloatNumber", ProviderType.File)]
        public float FloatNumber { get; set; }

        public ComponentBase(string name, TimeSpan time, int intNumber, float floatNumber, IProviderFactory providers)
            : base(providers)
        {
            Name = name;
            Time = time;
            IntNumber = intNumber;
            FloatNumber = floatNumber;
        }
    }
}
