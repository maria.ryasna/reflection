﻿using ConfigurationProvidersLibrary.Enums;
using System;

namespace Reflection.Entities.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class ConfigurationItemAttribute : Attribute
    {
        public string SettingName { get; }
        public ProviderType ProviderType { get; }
        public ConfigurationItemAttribute(string settingName, ProviderType providerType)
        {
            SettingName = settingName;
            ProviderType = providerType;
        }
    }
}
