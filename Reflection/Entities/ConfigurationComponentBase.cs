﻿using Reflection.Entities.Attributes;
using Reflection.Interfaces;
using System;
using System.Reflection;

namespace Reflection.Entities
{
    public abstract class ConfigurationComponentBase : IConfigurationComponentBase
    {
        private IProviderFactory _providers { get; set; }

        public ConfigurationComponentBase(IProviderFactory providers)
        {
            _providers = providers ?? throw new ArgumentException(nameof(providers));
        }
        public void SaveSettings()
        {
            var props = GetType().GetProperties();
            foreach (var prop in props)
            {
                var attribute = prop.GetCustomAttribute<ConfigurationItemAttribute>();
                var provider = _providers.GetProvider(attribute.ProviderType);

                var value = prop.GetValue(this);
                if (provider != null)
                {
                    provider.SaveSettings(attribute.SettingName, value);
                }
            }
        }
        public void LoadSettings()
        {
            var props = GetType().GetProperties();
            foreach (var prop in props)
            {
                var attribute = prop.GetCustomAttribute<ConfigurationItemAttribute>();
                var provider = _providers.GetProvider(attribute.ProviderType);
                if (provider != null)
                {
                    var value = provider.LoadSettings(prop, attribute.SettingName);
                    if (value != null)
                    {
                        prop.SetValue(this, provider.LoadSettings(prop, attribute.SettingName));
                    }
                }
            }
        }
    }
}
