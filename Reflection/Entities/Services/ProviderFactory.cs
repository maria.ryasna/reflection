﻿using ConfigurationProvidersLibrary.Enums;
using ConfigurationProvidersLibrary.Interfaces;
using Reflection.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Reflection.Entities.Services
{
    public class ProviderFactory : IProviderFactory
    {
        public ProviderFactory() { }
        public IConfigurationProvider GetProvider(ProviderType providerType)
        {
            var providers = LoadProviders(@"..\..\..\..\ProvidersLibrary.dll");

            if (providers != null)
            {
                var provider = providers.FirstOrDefault(provider => provider.ProviderType == providerType);

                provider.ParseService = new ParseService();
                if (providerType == ProviderType.File)
                {
                    provider.Path = @"..\..\..\..\FileConfig.txt";
                }
                return provider;
            }
            return null;
        }

        public IEnumerable<IConfigurationProvider> LoadProviders(string path)
        {
            try
            {
                var providers = new List<IConfigurationProvider>();

                var assembly = Assembly.LoadFrom(path);
                foreach (var providerType in assembly.GetTypes())
                {
                    if (providerType.GetInterfaces().Contains(typeof(IConfigurationProvider)))
                    {
                        var provider = Activator.CreateInstance(providerType) as IConfigurationProvider;
                        providers.Add(provider);
                    }
                }
                return providers;
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("[ERROR]: File with providers not found!");
            }
            return null;
        }
    }
}
