﻿using ConfigurationProvidersLibrary.Interfaces;
using System;
using System.Reflection;

namespace Reflection.Entities.Services
{
    public class ParseService : IParseService
    {
        public object Parse(PropertyInfo info, string value)
        {
            var type = info.PropertyType;
            if (type == typeof(string))
            {
                return value;
            }
            if (type == typeof(int))
            {
                return int.Parse(value);
            }
            if (type == typeof(float))
            {
                return float.Parse(value);
            }
            if (type == typeof(TimeSpan))
            {
                return TimeSpan.Parse(value);
            }
            return null;
        }
    }
}
