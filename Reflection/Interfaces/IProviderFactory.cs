﻿using ConfigurationProvidersLibrary.Enums;
using ConfigurationProvidersLibrary.Interfaces;

namespace Reflection.Interfaces
{
    public interface IProviderFactory
    {
        IConfigurationProvider GetProvider(ProviderType providerType);
    }
}
