﻿
namespace Reflection.Interfaces
{
    public interface IConfigurationComponentBase
    {
        void SaveSettings();
        void LoadSettings();
    }
}
