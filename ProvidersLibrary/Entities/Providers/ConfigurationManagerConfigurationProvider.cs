﻿using ConfigurationProvidersLibrary.Enums;
using ConfigurationProvidersLibrary.Interfaces;
using System;
using System.Configuration;
using System.Reflection;

namespace ProvidersLibrary.Entities.Providers
{
    public class ConfigurationManagerConfigurationProvider : IConfigurationProvider
    {
        public ProviderType ProviderType => ProviderType.ConfigurationManager;
        public IParseService ParseService { get; set; }
        public string Path { get; set; } = string.Empty;

        public ConfigurationManagerConfigurationProvider()
        {
        }

        public ConfigurationManagerConfigurationProvider(IParseService parseService) 
        {
            ParseService = parseService;
        }

        public object LoadSettings(PropertyInfo propInfo, string settingName)
        {
            var appSettings = ConfigurationManager.AppSettings;
            var temp = appSettings[settingName];
            if (appSettings.Count != 0 && appSettings[settingName] != null)
            {
                try
                {
                    return ParseService.Parse(propInfo, appSettings[settingName]);
                }
                catch (FormatException ex)
                {
                    Console.WriteLine("[ERROR]: Incorrect format! {0}", ex.Message);
                }
            }
            return null;
        }

        public void SaveSettings(string settingName, object value)
        {
            var appConfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var settings = appConfig.AppSettings.Settings;

            if (settings[settingName] == null)
            {
                settings.Add(settingName, value.ToString());
            }
            else
            {
                settings[settingName].Value = value.ToString();
            }

            appConfig.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection(appConfig.AppSettings.SectionInformation.Name);
        }
    }
}
