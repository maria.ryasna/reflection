﻿using ConfigurationProvidersLibrary.Enums;
using ConfigurationProvidersLibrary.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace ProvidersLibrary.Entities.Providers
{
    public class FileConfigurationProvider : IConfigurationProvider
    {
        public ProviderType ProviderType => ProviderType.File;
        public IParseService ParseService { get; set; }
        public string Path { get; set; }
        private string _separator = ";";

        public FileConfigurationProvider()
        {
        }

        public FileConfigurationProvider(string path, IParseService parseService)
        {
            Path = path;
            ParseService = parseService;
        }

        public object LoadSettings(PropertyInfo propInfo, string settingName)
        {
            var dictionary = GetFileValues();

            if (dictionary.ContainsKey(settingName))
            {
                try
                {
                    return ParseService.Parse(propInfo, dictionary[settingName]);
                }
                catch (FormatException ex)
                {
                    Console.WriteLine("[ERROR]: Incorrect format! {0}", ex.Message);
                } 
            }
            return null;
        }

        public void SaveSettings(string settingName, object value)
        {
            var dictionary = GetFileValues();

            if (dictionary.ContainsKey(settingName))
            {
                dictionary[settingName] = value.ToString();
            }
            else
            {
                dictionary.Add(settingName, value.ToString());
            }

            using (FileStream fs = new FileStream(Path, FileMode.Open))
            {
                using (StreamWriter writer = new StreamWriter(fs))
                {
                    foreach (var element in dictionary)
                    {
                        writer.WriteLine($"{element.Key};{element.Value}");
                    }
                }
            }
        }

        private Dictionary<string, string> GetFileValues()
        {
            var dictionary = new Dictionary<string, string>();
            using (FileStream fs = new FileStream(Path, FileMode.Open))
            {
                using (StreamReader reader = new StreamReader(fs))
                {
                    var text = reader.ReadLine();
                    while (text != null && text != string.Empty && text.Contains(_separator))
                    {
                        var data = text.Split(_separator);
                        var key = data[0];
                        var value = data[1];
                        if (dictionary.ContainsKey(key))
                        {
                            dictionary[key] = value;
                        }
                        else
                        {
                            dictionary.Add(key, value);
                        }
                        text = reader.ReadLine();
                    }
                }
            }
            return dictionary;
        }
    }
}
